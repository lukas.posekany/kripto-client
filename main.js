console.log("HELLO, I'M HERE")

fetch(
  "http://localhost:8080/pricesMatrixLast",
  {
    mode: 'cors',
    method: 'GET'
  }
  )
.then(response => response.json())
.then(matrix => {
  insertMatrix(matrix)
})

fetch(
  "http://localhost:8080/jumpsLast",
  {
    mode: 'cors',
    method: 'GET'
  }
  )
.then(response => response.json())
.then(matrix => {
  insertJumps(matrix)
})

function insertMatrix(matrix) {
  let table = document.getElementById("matrix")
  
  let headerRow = document.createElement("tr")
  headerRow.appendChild(document.createElement("td"))
  for (let currency of matrix.binancePairs) {
    const cell = document.createElement("td")
    cell.innerText = currency
    headerRow.appendChild(cell)
  }
  table.appendChild(headerRow)
  
  for (currencyI in matrix.binancePairs) {
    console.log(currencyI)

  }
  
}

function insertJumps(jump) {
  let table = document.getElementById("jumps")

  let trExchange = document.createElement("tr")
  let tdExchange = document.createElement("td")
  tdExchange.innerText = jump.exchange
  trExchange.appendChild(tdExchange)
  table.appendChild(trExchange)
  console.log(jump)

  for(let j of jump.jumpsList) {
    let trJumps = document.createElement("tr")
    let tdName = document.createElement("td")
    tdName.innerText = j.name 
    let tdJumps = document.createElement("td")
    tdJumps.innerText = j.jumps 
    let tdPercent = document.createElement("td")
    tdPercent.innerText = j.percent 
    trJumps.appendChild(tdName)
    trJumps.appendChild(tdJumps)
    trJumps.appendChild(tdPercent)
    table.appendChild(trJumps)
  }
}

// type Jump struct {
// 	Name    string   `json:"name"`
// 	Jumps   []string `json:"jumps"`
// 	Value   float32  `json:"value"`
// 	Percent float32  `json:"percent"`
// }

// type JumpCollection struct {
// 	Timestamp int64  `json:"timestamp"`
// 	Exchange  string `json:"exchange"`
// 	JumpsList []Jump `json:"jumpsList"`
// }